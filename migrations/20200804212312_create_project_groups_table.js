
const tableName = 'project_groups';

exports.up = (knex) => {
    return knex.schema.createTable(tableName, function(table) {
        table.string('project_id', 64).notNullable().index();
        table.string('group_id', 64).notNullable().index();
    });
};

exports.down = (knex) => {
    return knex.schema.dropTable(tableName);
};
