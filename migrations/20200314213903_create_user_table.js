
const table = 'users';

exports.up = function(knex) {
    return knex.schema.createTable(table, function(table) {
        table.string('uuid', 64).notNullable().unique();
        table.string('email', 190).notNullable().unique();
        table.string('password', 255).notNullable();

        table.string('name', 255).notNullable();
        table.string('lastname', 255).notNullable();
        table.string('company_id', 64).notNullable().index();

        table.datetime('deleted_at').defaultTo(null);
        table.datetime('created_at').defaultTo(null);
        table.datetime('updated_at').defaultTo(null);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable(table);
};
