
const tableName = 'groups';

exports.up = (knex) => {
    return knex.schema.createTable(tableName, function(table) {
        table.string('uuid', 64).notNullable().unique();
        table.string('user_id', 64).notNullable().index();
        table.string('name', 255).notNullable();

        table.text('info');

        table.datetime('created_at').defaultTo(null);
        table.datetime('updated_at').defaultTo(null);
    });
};

exports.down = (knex) => {
    return knex.schema.dropTable(tableName);
};
