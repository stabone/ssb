
const tableName = 'user_avatars';

exports.up = function(knex) {
    return knex.schema.createTable(tableName, function(table) {
        table.string('uuid', 64).notNullable().unique();
        table.string('user_id', 64).notNullable().index();
        table.string('file_name', 255).notNullable();
        table.json('info');
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable(tableName);
};
