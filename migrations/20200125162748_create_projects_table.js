
exports.up = function(knex) {
    return knex.schema.createTable('projects', function(table) {
        table.increments();

        table.string('uuid', 64).notNullable().unique();
        table.string('name', 255).notNullable();
        table.string('company_id', 64).notNullable().index();
        table.string('created_by', 64).notNullable().index();
        table.text('description');

        table.datetime('release_date').defaultTo(null);

        table.datetime('deleted_at').defaultTo(null);
        table.datetime('created_at').defaultTo(null);
        table.datetime('updated_at').defaultTo(null);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('projects');
};
