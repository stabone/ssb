
const tableName = 'user_groups';

exports.up = (knex) => {
    return knex.schema.createTable(tableName, function(table) {
        table.string('user_id', 64).notNullable().index();
        table.string('group_id', 64).notNullable().index();
    });
};

exports.down = (knex) => {
    return knex.schema.dropTable(tableName);
};
