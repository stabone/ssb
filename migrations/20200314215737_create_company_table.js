
const table = 'companies';

exports.up = function(knex) {
    return knex.schema.createTable(table, function(table) {
        table.string('uuid', 64).notNullable().unique();

        table.string('name', 255).notNullable();
        table.text('description');

        table.json('config');

        table.datetime('deleted_at').defaultTo(null);
        table.datetime('created_at').defaultTo(null);
        table.datetime('updated_at').defaultTo(null);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable(table);
};
