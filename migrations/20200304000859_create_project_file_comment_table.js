
exports.up = function(knex) {
    return knex.schema.createTable('project_file_comments', (table) => {
        table.string('uuid', 64).notNullable().unique();

        table.string('project_file_id', 64).notNullable().index();
        table.string('parent_id', 64).nullable().index();

        table.text('comment');
        table.json('additional');
        table.string('added_by').notNullable();

        table.timestamp('created_at').defaultTo(null);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('project_file_comments');
};
