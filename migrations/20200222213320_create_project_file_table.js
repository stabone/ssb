
exports.up = function(knex) {
    return knex.schema.createTable('project_files', (table) => {
        table.string('uuid', 64).notNullable().unique();

        table.string('project_id', 64).notNullable().index();
        table.string('file_name', 255).notNullable();
        table.string('original_file_name', 255).notNullable();

        table.json('additional');
        // table.string('company_id', 64).notNullable().index();
        table.string('added_by', 64).notNullable().index();

        table.timestamp('created_at').defaultTo(null);
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('project_files');
};
