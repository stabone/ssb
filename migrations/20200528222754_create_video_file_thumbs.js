const table = 'project_file_thumbs';

exports.up = (knex) => {
    return knex.schema.createTable(table, (table) => {
        table.string('uuid', 64).notNullable().unique();

        table.string('project_file_id', 64).notNullable().index();
        table.string('file_name', 255).notNullable();
    });
};

exports.down = (knex) => {
    return knex.schema.dropTable(table);
};
