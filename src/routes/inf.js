const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const fs = require('fs');
const path = require('path');

const Project = require('../repositories/project');
const ProjectFile = require('../repositories/project_file');

const router = express.Router();

const plans = [ 'Free', 'Basic', 'PRO', 'Off the charts' ];

router.route('/')
    .get(async (req, res) => {
        const { user } = res.locals;

        const mbConst = 0.000001;
        const files = await ProjectFile.find({ added_by: user.id });

        console.log(files);

        const total = files.reduce((accum, curr) => {
            const add = JSON.parse(curr.additional);

            return accum + (add.size * mbConst);
        }, 0);

        res.json({
            plan: 'Basic',
            usedSpace: total,
        });
    });

module.exports = router;
