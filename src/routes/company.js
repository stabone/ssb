const express = require('express');

const Company = require('../models/company');
const { getDataFromRequest } = require('../helpers/request');

const router = express.Router();

const fields = [
    'name',
    'about',
    'website',
    'phone',
    'email',
    'address',
    'logo',
];

router.route('/')
    .get((req, res) => {
        Company.find({ deleted_at: null }, (err, docs) => res.json(docs));
    })
    .post((req, res) => {
        const data = getDataFromRequest(req.body, fields);

        Company.create(data)
            .then(company => res.json(company))
            .catch(err => res.status(500).json({ error: '' }))
    });

router.param('id', (req, res, next, id) => {
    Company.findOne({ _id: id }, (err, company) => {
        company = err ? {} : (company == undefined ? {} : company);

        req.company = company;

        next();
    });
});

router.route('/:id')
    .get((req, res) => {
        res.json(req.company);
    })
    .put(async (req, res) => {
        const { _id } = req.company;

        const data = getDataFromRequest(req.body, fields);

        await Company.updateOne({ _id }, data);

        return res.status(201).json(req.company);
    })
    .delete(async (req, res) => {
        const { _id } = req.company;

        await Company.updateOne({ _id }, { deleted_at: new Date() });

        return res.json(req.company);
    });

module.exports = router;