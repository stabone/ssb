const express = require('express');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const fs = require('fs');
const path = require('path');

const User = require('../repositories/user');
const UserAvatar = require('../repositories/user_avatar');
const { timeThings } = require('../functions/jwt');
const { privateKeyPath } = require('../helpers/storage');

const router = express.Router();

const privateKey = fs.readFileSync(privateKeyPath, 'utf8');


const passwordMessage = 'Password field must have the same value as the password field';

const getRequestData = (requestData) => {
    const formData = Buffer.from(requestData, 'base64').toString('utf-8');

    return JSON.parse(formData);
};

router.route('/l')
    .post(async (req, res) => {
        const formData = getRequestData(req.body.form);

        if (Object.keys(formData).length !== 2) {
            return res.json({ token: null, errors: [ 'No data' ] });
        }

        const { email, password } = formData;

        const user = await User.getByEmail(email);

        if (!user || !bcrypt.compareSync(password, user.password)) {
            return res.json({ token: null });
        }

        const avatar = await UserAvatar.findOne({ user_id: user.uuid });

        let payload = {
            id: user.uuid,
            user: `${user.name} ${user.lastname}`,
            name: user.name,
            lastname: user.lastname,
            avatar: (avatar ? avatar.uuid : null),
            email: user.email,
            permissions: {},
            ...timeThings()
        };

        jwt.sign(payload, privateKey, {}, (err, token) => {
            // varbūt piereģistrēt izsniegtos tokenus???
            res.json({ token: (!err ? token : null) });
        });
    });

router.route('/r')
    .post(async (req, res) => {
        let obj = { };

        const body = getRequestData(req.body.form);
        const formFields = [
            'name', 'lastname', 'email', 'password'
        ];

        formFields.forEach(field => {
            obj[ field ] = body[ field ];
        });

        obj.company_id = 0;
        obj.password = bcrypt.hashSync(obj.password, bcrypt.genSaltSync(10));

        try {
            await User.insert(obj);
            const user = await User.getByEmail(obj.email);
            const { uuid, name, lastname, email } = user;

            const payload = {
                id: uuid,
                user: `${name} ${lastname}`,
                name,
                lastname,
                avatar: null,
                email,
                ...timeThings()
            };

            jwt.sign(payload, privateKey, {}, (err, token) => {
                // varbūt piereģistrēt izsniegtos tokenus???
                res.json({ token: (!err ? token : null) });
            });
        } catch (err) {
            console.log(err);

            if (err['sqlMessage']) err = 'Server issue';

            res.sendStatus(500).json({ error: err });
        }
    });

router.route('/cp')
    .post(async (req, res) => {
        const { password, rePassword } = req.body;
        const { user } = res.locals;

        try {
            // if (password.length < 6 || re_password.length < 6) throw "Invalid password";
            if (password !== rePassword) throw "Invalid password";

            const foundUser = await User.getById(user.id);

            if (foundUser === undefined) throw "Invalid user data";

            const newPassword = bcrypt.hashSync(password, bcrypt.genSaltSync(10));

            if (foundUser.password !== newPassword) {
                User.updatePassword(user.id, newPassword);
            }
        } catch (e) {
            return res.status(500).json({ error: e });
        }

        res.json({ ...user });
    });

module.exports = router;
