const express = require('express');
const { extname } = require('path');
const { statSync, createReadStream, unlink } = require('fs');

const multer = require('multer');
const { v4: uuidv4 } = require('uuid');


const knex = require('../knex');
const {
    videoPathDir, videoPath,
    thumbPathDir, thumbPath
} = require('../helpers/storage');
const { createThumbnail } = require('../tasks/thumb');
const { getMimeType } = require('../functions/mime');
const {
    projectFileRecord,
    getCommentRecord,
    getThumbId,
    getGroupIds,
    cutDescription,
    getProjectIds,
} = require('../functions/project');

const User = require('../repositories/user');
const Project = require('../repositories/project');
const ProjectFile = require('../repositories/project_file');
const ProjectFileThumb = require('../repositories/project_file_thumbs');
const ProjectFileComment = require('../repositories/project_file_comment');
const ProjectGroup = require('../repositories/project_group');
const UserGroup = require('../repositories/user_group');

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, videoPathDir);
    },
    filename: (req, file, cb) => {
        const dateNow = Date.now();
        const ext = extname(file.originalname);
        const fileId = uuidv4();

        cb(null, `${dateNow}_${fileId}${ext}`);
    }
});

const upload = multer({ storage }).array('files', 10);

router.route('/')
    .get(async (req, res) => {
        const { user } = res.locals;

        try {
            const userGroups = await UserGroup.find({ user_id: user.id });
            const onlyGroups = userGroups.map(userGroup => userGroup.group_id);

            const projectGroups = await ProjectGroup.findIn('group_id', onlyGroups);
            const projectIds = getProjectIds(projectGroups);
            const fields = [ 'id', 'uuid', 'name', 'description', 'created_by' ];

            let items = [];
            const projects = await Project.findInOr('uuid', projectIds, { created_by: user.id }, fields);

            for (let ii = 0; ii < projects.length; ++ii) {
                let project = { ...projects[ii] };
                const file = await ProjectFile.findOne({ project_id: project.uuid });
                const thumb = await ProjectFileThumb.findOne({
                    project_file_id: file.uuid
                });
                const groupIds = await ProjectGroup.find({ project_id: project.uuid });

                project.description = cutDescription(project.description);
                project.thumb_id  = getThumbId(thumb);
                project.group_ids = getGroupIds(groupIds);

                items.push(project);
            }

            return res.json(items);
        } catch (e) {
            console.log('eee', e.message);
            return res.status(500);
        }
    })
    .post(upload, async (req, res) => {
        const { body, files } = req;
        const { name, description } = body;
        const { user } = res.locals;
        const uid = uuidv4();

        await Project.insert({
            name, description,
            uuid: uid,
            company_id: 1,
            created_by: user.id // TODO: this should be user uuid
        });

        files.forEach(async (file) => {
            const {
                originalname, encoding, mimetype,
                destination, filename, size
            } = file;
            const additional = JSON.stringify({
                encoding, mimetype, size, destination
            });

            const data = projectFileRecord(uid, filename, originalname, additional, user.id);

            createThumbnail(videoPath(filename), thumbPathDir)
                .then(thumbFiles => {
                    (thumbFiles || []).forEach(thumbFile => {
                        ProjectFileThumb.insert({
                            project_file_id: data.uuid,
                            file_name: thumbFile
                        })
                        .then(file => console.log('Added thumb:', file))
                        .catch(err => console.log('Thumb err:', err));
                    })
                })
                .catch(err => console.log('Thumb File Err:', err));

            await ProjectFile.insert(data);
        });

        res
            .status(200)
            .json(['ok'])
    });

router.route('/:id')
    .get(async (req, res) => {
        const projectId = req.params.id;
        const { user } = res.locals;

        let project = await Project.findOne({ uuid: projectId, created_by: user.id });
        let usersList = await User.list();

        let users = {};
        usersList.forEach(user => {
            users[user.uuid] = `${user.name} ${user.lastname}`;
        })

        const fields = [ 'uuid', 'original_file_name', 'created_at' ];
        let projectFiles = await ProjectFile.getByProjectId(projectId, fields);

        project.files = [];
        project.comments = {};

        for (let ii = 0; ii < projectFiles.length; ++ii) {
            const file = projectFiles[ii];
            const comments = await ProjectFileComment.getByFileId(file.uuid);
            const thumb = await ProjectFileThumb.findFileThumb(file.uuid);
            file.thumb_id = thumb ? thumb.uuid : null;

            project.files.push(file);
            project.comments[file.uuid] = (comments || []).map(item => getCommentRecord(item, users));
        }

        res.json(project);
    })
    .post(upload, async (req, res) => {
        const { body, files, params } = req;
        const { name, description } = body;
        const { user } = res.locals;
        const { id } = params;

        let fields = {};
        [ 'name', 'description' ].forEach(field => {
            if (body[field]) fields[field] = body[field];
        });

        if (Object.keys(fields).length > 0) {
            await Project.update({ uuid: id }, fields);
        }

        (files || []).forEach(async (file) => {
            const {
                originalname, encoding, mimetype,
                destination, filename, size
            } = file;
            const additional = JSON.stringify({
                encoding, mimetype, size, destination
            });

            const data = projectFileRecord(id, filename, originalname, additional, user.id);

            createThumbnail(videoPath(filename), thumbPathDir)
                .then(thumbFiles => {
                    (thumbFiles || []).forEach(thumbFile => {
                        ProjectFileThumb.insert({
                            project_file_id: data.uuid,
                            file_name: thumbFile
                        })
                        .then(file => console.log('Added thumb:', file))
                        .catch(err => console.log('Thumb err:', err));
                    })
                })
                .catch(err => console.log('Thumb File Err:', err));

            ProjectFile.insert(data)
                .then(projectFile => console.log('Project File:', projectFile))
                .catch(e => console.error('Project File Error:', e));
        });

        const project = await Project.findOne({ uuid: id });

        res.status(200)
            .json(project);
    })
    .put(async (req, res) => {
        const { _id } = req.project;

        const data = getDataFromRequest(req.body, fields);

        // await Project.updateOne({ _id }, data);

        return res.status(201).json(req.project);
    })
    .delete(async (req, res) => {
        const { id }   = req.params;
        const { user } = res.locals;

        try {
            const project = await Project.findOne({
                uuid: id, created_by: user.id
            });
            if (project === undefined) throw 'Project error';

            const projectFiles = await ProjectFile.find({ project_id: id });

            await Project.delete({ uuid: id });

            projectFiles.forEach(async (file) => {
                const fileId = file.uuid;

                const thumbs = await ProjectFileThumb.find({ project_file_id: fileId });

                await ProjectFileThumb.delete({ project_file_id: fileId });
                await ProjectFileComment.delete({ project_file_id: fileId });
                await ProjectFile.delete({ uuid: fileId });

                thumbs.forEach(thumb => {
                    unlink(thumbPath(thumb.file_name), (err) => {
                        if (err) console.error('Cant delete thumb:', err);
                    });
                });

                unlink(videoPath(file.file_name), (err) => {
                    if (err) console.error('Cant delete video:', err);
                });
            });

            return res.json({ id });
        } catch (e) {
            return res.status(500).json({ error: e.message });
        }
    });

// TODO: move to lib
router.route('/t/:id')
    .get(async (req, res) => {
        const uuid = req.params.id;

        try {
            const thumb = await ProjectFileThumb.findOne({ uuid })
            const imageFile = thumbPath(thumb.file_name);
            const stats = statSync(imageFile);

            res.setHeader('Content-Type', getMimeType(thumb.file_name));
            res.status(200);

            const stream = createReadStream(imageFile, { start: 0, end: stats.size - 1 });
            stream.on('open', () => stream.pipe(res));
            stream.on('error', (streamErr) => res.end(streamErr));
        } catch (err) {
            console.log(err);

            return res.status(404).json([]);
        }
    });

router.route('/group/:id')
    .post(async (req, res) => {
        const { body, params: { id } } = req;
        const { user } = res.locals;
        const { projectId, groupIds } = body;

        let groups = [ ];

        if (id !== projectId) return res.status(500);

        await ProjectGroup.delete({ project_id: projectId });

        for (let ii = 0; ii < groupIds.length; ++ii) {
            const item = { project_id: projectId, group_id: groupIds[ii] };
            await ProjectGroup.insert(item);

            groups.push(item);
        }

        res.json(groups);
    });

module.exports = router;
