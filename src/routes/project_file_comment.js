const express = require('express');
const uuid = require('uuid');

const User = require('../repositories/user');
const ProjectFile = require('../repositories/project_file');
const ProjectFileComment = require('../repositories/project_file_comment');

const router = express.Router();


router.route('/comment/:id')
    .post(async (req, res) => {
        const { id } = req.params;
        const { comment, parent_id } = req.body;
        let playerRange = req.body.playerRange || {};
        const { user } = res.locals;

        const file = await ProjectFile.getById(id);
        const currentUser = await User.getById(user.id);

        const data = {
            uuid: uuid.v4(),
            project_file_id: file.uuid,
            comment,
            added_by: user.id,
            parent_id: parent_id || null,
        };

        try {
            await ProjectFileComment.insert({
                ...data, additional: JSON.stringify(playerRange)
            });

            data.added_by = `${currentUser.name} ${currentUser.lastname}`;
            data.start = playerRange.start;
            data.end = playerRange.end;

            res.json(data);
        } catch (e) {
            console.log(e)
            res.status(500).json([ e ]);
        }
    });

module.exports = router;
