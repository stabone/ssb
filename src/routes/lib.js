const express = require('express');
const { statSync, accessSync, constants, createReadStream } = require('fs');
const { pipeline } = require('stream');

const UserAvatar = require('../repositories/user_avatar');
const ProjectFile = require('../repositories/project_file');

const videoHelpers = require('../helpers/videoHelpers');
const { avatarSmallPath, videoPath } = require('../helpers/storage');
const { getMimeType } = require('../functions/mime');
const logger = require('../helpers/logger');

const { F_OK, R_OK } = constants;

const router = express.Router()

const streamImage = (imageFile, res) => {
    const stats = statSync(imageFile);

    res.setHeader('Content-Type', getMimeType(imageFile));
    res.status(200);

    const stream = createReadStream(imageFile, { start: 0, end: stats.size - 1 });
    // stream.on('end', () => console.log('  Stream ended:', videoFile));
    // stream.on('close', () => console.log('  Stream closed', videoFile))

    pipeline(stream, res, (err) => {
        if (err) console.log('U A err:', err)
    });
};

router.route('/u/a/:id')
    .get(async (req, res) => {
        const { id } = req.params;

        try {
            logger.info(`Avatar uuid: ${id}`);
            const thumb = await UserAvatar.getById(id);
            if (thumb === undefined) throw 'file not found';

            const imageFile = avatarSmallPath(thumb.file_name);

            streamImage(imageFile, res);;
        } catch (err) {
            console.log(err);
            logger.error(err);

            return res.status(404).json([]);
        }
    });

router.route('/u/s/:id')
    .get(async (req, res) => {
        const { id } = req.params;

        try {
            logger.info(`Avatar small uuid: ${id}`);
            const thumb = await UserAvatar.findOne({ user_id: id });

            if (thumb === undefined) throw 'file not found';

            const imageFile = avatarSmallPath(thumb.file_name);

            streamImage(imageFile, res);
        } catch (err) {
            console.log(err);
            logger.error(err);

            return res.status(404).json([]);
        }
    });

router.route('/v/:uuid')
    .get(async (req, res) => {
        const { uuid } = req.params;

        try {
            const projectFiles = await ProjectFile.findOne({ uuid });
            const videoFile = videoPath(projectFiles.file_name);

            accessSync(videoFile, F_OK | R_OK );

            const stats = statSync(videoFile);
            const readInfo = videoHelpers.getFileReadingInfo(req.headers, stats, videoFile);

            res.set(videoHelpers.getChunkHeader(readInfo));
            res.status(206);

            const stream = createReadStream(videoFile, { start: readInfo.start, end: readInfo.end });
            // stream.on('end', () => console.log('  Stream ended:', videoFile));
            // stream.on('close', () => console.log('  Stream closed', videoFile));

            pipeline(stream, res, (err) => {
                if (err) console.log('Stream error:', err)
            });
        } catch (err) {
            console.log(err);

            return res.end(err);
        }
    });

module.exports = router
