const express = require('express');
const { v4: uuidv4 } = require('uuid');

const logger = require('../helpers/logger');
const User = require('../repositories/user');
const Group = require('../repositories/group');
const UserGroup = require('../repositories/user_group');
const ProjectGroup = require('../repositories/project_group');

const router = express.Router();

const createUserMap = (users) => {
    let userMap = {};

    users.forEach(usr => { userMap[usr.uuid] = { ...usr }; })

    return userMap;
};

router.route('/')
    .get(async (req, res) => {
        const { user } = res.locals;

        try {
            const userFields = [ 'uuid', 'name', 'lastname' ];
            const ids      = new Set();
            const groups   = await Group.find({ user_id: user.id });
            const groupIds = groups.map(group => group.uuid);
            const userGroups = await UserGroup.findIn('group_id', groupIds);

            let usersInGroup = { };
            userGroups.forEach(userGroup => {
                if (!usersInGroup[userGroup.group_id]) usersInGroup[userGroup.group_id] = [ ];

                usersInGroup[userGroup.group_id].push(userGroup.user_id);
                ids.add(userGroup.user_id);
            });

            const users = await User.findIn('uuid', [ ...ids ], userFields);

            const userMap = createUserMap(users);
            const ww = groups.map(group => {
                group.users = (usersInGroup[group.uuid] || []).map(uid => userMap[uid].uuid);

                return group;
            });

            res.json(ww);
        } catch (e) {
            console.log(e);
            res.status(500).json({ error: e.message });
        }
    })
    .post(async (req, res) => {
        const { body: { name, info } } = req;
        const { user } = res.locals;

        try {
            const uuid = uuidv4();
            const data = {
                uuid, name, info, user_id: user.id
            };

            const group = await Group.insert(data);
            await UserGroup.insert({ group_id: uuid, user_id: user.id });

            res.json({
                id: data.uuid, name: data.name, info: data.info
            });
        } catch (e) {
            console.log(e);
            res.status(500).json({ error: e.message });
        }
    });

router.route('/my')
    .get(async (req, res) => {
        const { id } = req.params;
        const { user } = res.locals;

        logger.info(`Get my groups ${user.id}`);
        const fields = [ { id: 'uuid' }, 'name', 'info' ];

        try {
            const ids = new Set()
            const groups     = await Group.find({ user_id: user.id }, fields);
            const groupIds   = groups.map(group => group.id);
            const userGroups = await UserGroup.findIn('group_id', groupIds);

            let usersInGroup = { };
            userGroups.forEach(userGroup => {
                if (!usersInGroup[userGroup.group_id]) usersInGroup[userGroup.group_id] = [ ];

                usersInGroup[userGroup.group_id].push(userGroup.user_id);
                ids.add(userGroup.user_id);
            });

            const users = await User.findIn('uuid', [ ...ids ], [ 'uuid' ]);

            const userMap = createUserMap(users);
            const ww = groups.map(group => {
                group.users = (usersInGroup[group.id] || []).map(uid => userMap[uid].uuid);

                return group;
            });

            res.json(ww)
        } catch (exc) {
            res.status(500);
        }
    });

router.route('/:id')
    .get(async (req, res) => {
        const { id } = req.params;
        const { user } = res.locals;

        try {
            const group = await Group.findOne({
                uuid: id, user_id: user.id
            });
            const groupUsers = await UserGroup.find({ group_id: group.id });

        } catch (exc) {

        }
    })
    .put(async (req, res) => {
        const { id }   = req.params;
        const { user } = res.locals;

        try {
            const group = await Group.findOne({
                uuid: id, user_id: user.id
            });

            if (!group) throw `Group '${id}' not found`;

            const { name, info, users: userIds } = req.body;

            let uniqueIds = new Set([ ...userIds ]);
            uniqueIds.add(user.id);

            await Group.update({ uuid: group.uuid }, { name, info });
            await UserGroup.delete({ group_id: group.uuid });
            uniqueIds.forEach(async (userId) => {
                await UserGroup.insert({
                    group_id: group.uuid,
                    user_id: userId
                });
            });

            res.json({
                id: group.uuid,
                name,
                info,
                users: [ ...uniqueIds ],
            });
        } catch (e) {
            res.status(500);
        }
    })
    .delete(async (req, res) => {
        const { id }   = req.params;
        const { user } = res.locals;

        try {
            const group = await Group.findOne({
                uuid: id, user_id: user.id
            });

            if (group) {
                await Group.delete({ uuid: group.uuid });
                await UserGroup.delete({ group_id: group.uuid });
            }

            res.json({ id: group.uuid, name: group.name });
        } catch (e) {
            res.status(500);
        }
    });

module.exports = router;
