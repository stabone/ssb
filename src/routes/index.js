
const projectRoutes  = require('./project');
const projectFileRoutes = require('./project_file_comment');
const groupRoutes = require('./group');
const authRoutes = require('./auth');
const userRoutes = require('./user');
const infRoutes = require('./inf');
const libRoutes = require('./lib');

const apiV1 = (path) => `/v1/${path}`;

module.exports = (app) => {
    app.use(apiV1('a'),            authRoutes);
    app.use(apiV1('projects'),     projectRoutes);
    app.use(apiV1('project-file'), projectFileRoutes);
    app.use(apiV1('group'),        groupRoutes);
    app.use(apiV1('user'),  userRoutes);
    app.use(apiV1('inf'),   infRoutes);
    app.use(apiV1('lib'),   libRoutes);
};
