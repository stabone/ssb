const path = require('path');
const { unlink, existsSync } = require('fs');

const express = require('express');
const multer  = require('multer');
const uuid    = require('uuid');
const jimp    = require('jimp');

const {
    avatarPathDir, avatarPath,
    avatarSmallPath,
} = require('../helpers/storage');

const User = require('../repositories/user');
const UserAvatar = require('../repositories/user_avatar');
const { getDataFromRequest } = require('../helpers/request');


const router = express.Router();

const fields = [ 'name', 'lastname' ];
const userIdCheck = (user, params) => {
    if (user.id !== params.id) {
        throw 'Invalid user information';
    }
};

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, avatarPathDir);
    },
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        const fileId = uuid.v4();

        cb(null, `${fileId}${ext}`);
    }
});

const removePreviousFiles = (items = []) => {
    items.forEach(item => {
        const fileName = item.file_name;
        const filePaths = [ avatarPath(fileName), avatarSmallPath(fileName) ];

        filePaths.forEach(filePath => {
            if (!existsSync(filePath)) return;

            unlink(filePath, err => console.log('File Delete:', err));
        });
    });
};

const writeSmallAvatar = (fileName) => {
    jimp.read(avatarPath(fileName))
        .then(image => {
            image
                .cover(256, 256, jimp.HORIZONTAL_ALIGN_CENTER | jimp.VERTICAL_ALIGN_MIDDLE)
                .crop(100, 94, 64, 64)
                .quality(90)
                .write(avatarSmallPath(fileName));
        })
        .catch(err => console.error(err));
};

const upload = multer({ storage }).single('image');

router.route('/')
    .get((req, res) => {
        User.find({ deleted_at: null }, [ { id: 'uuid' }, 'email', 'name', 'lastname' ])
            .then(users => res.json(users))
            .catch(err => res.status(500));
    })
    .post((req, res) => {
        const data = getDataFromRequest(req.body, fields);

        User.create(data)
            .then(company => res.json(company))
            .catch(err => res.status(500).json({ error: '' }));
    });

// router.param('id', (req, res, next, id) => {
//     User.findOne({ _id: id }, (err, company) => {
//         company = err ? {} : (company == undefined ? {} : company);
//
//         req.company = company;
//
//         next();
//     });
// });

router.route('/:id')
    .put(upload, async (req, res) => {
        const { params, file } = req;
        const { user } = res.locals;

        try {
            userIdCheck(user, params);
            console.log('###', user)

            const data = getDataFromRequest(req.body, fields);
            await User.update({ uuid: user.id }, data);

            if (file) {
                const avatarId = uuid.v4();
                const avatar = {
                    uuid: avatarId,
                    info: JSON.stringify(file),
                    user_id: user.id,
                    file_name: file.filename
                };

                writeSmallAvatar(file.filename);

                const items = await UserAvatar.find({ user_id: user.id });

                removePreviousFiles(items);

                await UserAvatar.delete({ user_id: user.id });
                await UserAvatar.insert(avatar);
                data.avatar = avatarId;
            }

            res.status(201).json(data);
        } catch (err) {
            console.log(err)
            res.status(500).json({ msg: err });
        }
    })
    .delete(async (req, res) => {
        const { params } = req;
        const { user } = res.locals;

        try {
            userIdCheck(user, params);

            await User.updateOne({ _id }, { deleted_at: new Date() });

            return res.json(req.company);
        } catch (err) {
            console.log(err)
            res.status(500).json({ msg: err });
        }
    });

module.exports = router;
