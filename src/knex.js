const knexConfig = require('../knexfile');

const environment = process.env.ENVIRONMENT || 'development';
const config = knexConfig[environment];

module.exports = require('knex')({
    client: config.client,
    connection: config.connection
});