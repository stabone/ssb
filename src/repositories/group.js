const BaseRepository = require('./BaseRepository');


class Group extends BaseRepository {
    static tableName() {
        return 'groups';
    }

    static insert(data) {
        data.created_at = new Date();
        data.updated_at = null;

        return this.table().insert(data);
    }

    static update(filter, data) {
        data.updated_at = new Date();

        return this.find(filter)
            .update(data);
    }

    static findIn(field, ids) {
        return this.table()
            .column([ { id: 'uuid' }, 'name', 'info', 'created_at' ])
            .whereIn(field, ids);
    }
}

module.exports = Group;
