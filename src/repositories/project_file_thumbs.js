const { v4: uuidv4 } = require('uuid');
const BaseRepository = require('./BaseRepository');


class ProjectFileThumbs extends BaseRepository {
    static tableName() {
        return 'project_file_thumbs';
    }

    static insert(data) {
        data.uuid = uuidv4();

        return this.table().insert(data);
    }

    static findOne(whereCondition) {
        return this.find(whereCondition)
            .orderBy('file_name', 'desc')
            .first();
    }

    static findFileThumb(fileId) {
        return this.findOne({ project_file_id: fileId });
    }
}

module.exports = ProjectFileThumbs;
