const { v4: uuidv4 } = require('uuid');
const BaseRepository = require('./BaseRepository');


class User extends BaseRepository {
    static tableName() {
        return 'users';
    }

    static insert(data) {
        data.uuid = uuidv4();
        data.created_at = new Date();
        data.updated_at = null;

        return this.table().insert(data);
    }

    static getByEmail(email) {
        return this.findOne({ email });
    }

    static all() {
        return this.table().select();
    }

    static list() {
        return this.table()
            .select(['uuid', 'name', 'lastname']);
    }

    static updatePassword(uuid, password) {
        return this.update({ uuid }, { password });
    }

    static delete(uuid) {
        return this.update({ uuid }, { deleted_at: new Date() });
    }
}

module.exports = User;
