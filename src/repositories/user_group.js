const BaseRepository = require('./BaseRepository');

class UserGroup extends BaseRepository {
    static tableName() {
        return 'user_groups';
    }
}

module.exports = UserGroup;
