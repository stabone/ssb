const uuid = require('uuid');
const BaseRepository = require('./BaseRepository');


class UserAvatar extends BaseRepository {
    static tableName() {
        return 'user_avatars';
    }

    static all() {
        return this.table();
    }

    static insert(data) {
        if (!data.uuid) {
            data.uuid = uuid.v4();
        }

        return this.table()
            .insert(data);
    }

    static getByEmail(email) {
        return this.table()
            .first()
            .where({ email });
    }

    static update(condition, updateStmt) {
        return this.table()
            .where(condition)
            .update(updateStmt);
    }
}

module.exports = UserAvatar;
