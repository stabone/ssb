const BaseRepository = require('./BaseRepository');


class Project extends BaseRepository {
    static tableName() {
        return 'project_files';
    }

    static insert(data) {
        data.created_at = new Date();
        // data.updated_at = null;

        return this.table().insert(data);
    }

    static getById(uuid) {
        return this.table()
            .where({ uuid }).first([ 'uuid', 'file_name' ]);
    }

    static getByProjectId(projectId, fields = []) {
        return this.table()
            .where({ project_id: projectId })
            .select(fields.length > 0 ? fields : '*');
    }

    static getByCompany(comapnyId) {
        return this.table()
            .select([ 'uuid', 'name', 'created_at' ])
            .where({ company_id: comapnyId });
    }

    static async destroyByProject(id) {
        return this.table()
            .where({ project_id: id })
            .del();
    }
}

module.exports = Project;
