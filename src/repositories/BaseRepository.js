const knex = require('../knex');


class BaseRepository {
    static tableName() {
        return '';
    }

    static table() {
        return knex(this.tableName());
    }

    static insert(data) {
        return this.table().insert(data);
    }

    static find(condition, fields = null) {
        const table = this.table();

        if (fields) table.column(fields);

        return table.where(condition);
    }

    static findIn(field, ids, fields = [ ]) {
        let query = this.table()
            .whereIn(field, ids);

        if (fields.length) query.select(fields);

        return query;
    }

    static findOne(condition) {
        return this.find(condition).first();
    }

    static getById(uuid) {
        return this.table()
            .where({ uuid }).first();
    }

    static update(condition, updateStmt) {
        return this.find(condition)
            .update(updateStmt);
    }

    static delete(condition) {
        return this.find(condition).del();
    }
}

module.exports = BaseRepository;
