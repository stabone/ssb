const knex = require('../knex');
const BaseRepository = require('./BaseRepository');

class Project extends BaseRepository {
    static tableName() {
        return 'project_file_comments';
    }

    static insert(data) {
        data.created_at = new Date();

        return this.table().insert(data);
    }

    static getById(uuid) {
        return this.table()
            .where({ uuid }).first([
                'uuid', 'project_file_id', 'comment', 'additional'
            ]);
    }

    static getByFileId(uuid) {
        return this.table()
            .where({ project_file_id: uuid }).select();
    }

    static getByCompany(comapnyId) {
        return this.table()
            .select([ 'uuid', 'name', 'created_at' ])
            .where({ company_id: comapnyId });
    }
}

module.exports = Project;
