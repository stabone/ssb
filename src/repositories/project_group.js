const BaseRepository = require('./BaseRepository');

class ProjectGroup extends BaseRepository {
    static tableName() {
        return 'project_groups';
    }

    static insert(data) {
        return this.table().insert(data);
    }
}

module.exports = ProjectGroup;
