const BaseRepository = require('./BaseRepository');


class Project extends BaseRepository {
    static tableName() {
        return 'projects';
    }

    static insert(data) {
        data.created_at = new Date();
        data.updated_at = null;

        return this.table().insert(data);
    }

    static update(filter, data) {
        data.updated_at = new Date();

        return this.find(filter)
            .update(data);
    }

    static getById(uuid) {
        return this.table()
            .where({ uuid }).first([ 'uuid', 'name', 'description' ]);
    }

    static findInOr(field, ids, orCondition, fields = [ ]) {
        let query = this.table()
            .whereIn(field, ids)
            .orWhere(orCondition);

        if (fields.length) query.select(fields);

        return query;
    }

    static getByCompany(comapnyId) {
        return this.table()
            .select([ 'uuid', 'name', 'description', 'created_at' ])
            .where({ company_id: comapnyId })
            .orderBy('created_at', 'desc');
    }
}

module.exports = Project;
