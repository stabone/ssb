require('dotenv').config({ path: __dirname + '/../.env' });

const fs   = require('fs');
const path = require("path");

const express = require("express");
const jwt  = require('jsonwebtoken');
const cors = require('cors');

const jwtHelpers = require('./helpers/jwt');
const {
    privateKeyPath,
    publicKeyPath
} = require('./helpers/storage');
const projectSetup = require('./project_setup.js');

projectSetup();

// for dev http://travistidwell.com/jsencrypt/demo/
const privateKEY = fs.readFileSync(privateKeyPath, 'utf8');
const publicKEY  = fs.readFileSync(publicKeyPath, 'utf8');

const routes = require('./routes');

const port = process.env.SERVER_PORT || 3030;
const app = express();
app.disable('etag')
    .disable('x-powered-by');

const corsOptions = {
    origin: process.env.CORS_ORIGIN,
    optionsSuccessStatus: 200,
};
console.log(corsOptions)

// app.use(express.json({ limit: '32mb' }));
// app.use(express.urlencoded({ extended: true, limit: '32mb' }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(cors(corsOptions));
app.use((req, res, next) => {
    const origUrl = req.originalUrl;

    const isLib = origUrl.startsWith('/v1/lib/');
    const isThumb = origUrl.startsWith('/v1/projects/t');

    const isAuth = [ '/v1/a/l', '/v1/a/r' ].includes(origUrl);

    if (isAuth || isLib || isThumb) {
        next();

        return;
    }

    const authorization = req.header('authorization');


    if (authorization) {
        jwt.verify(authorization.substr('Bearer '.length), privateKEY, (err, decoded) => {
            if (err) {
                return res.sendStatus(401).json({
                    success: false,
                    message: 'Authentication failed! Please check the request'
                });
            } else {
                res.locals.user = decoded;

                next();
            }
        });
    } else {
        return res.sendStatus(400).json({
            success: false,
            message: 'Authentication failed! Please check the request'
        });
    }
});

app.get("/", (req, res) => {
    res.send({ status: 'ok' });
});

routes(app);

app.listen(port, () => {
    // https://developer.okta.com/blog/2018/11/15/node-express-typescript
    console.log(`Listening on port: ${port}`);
});
