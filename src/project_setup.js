const { exists, mkdir } = require('fs');
const path = require('path');

module.exports = () => {
    const storageBase = [ __dirname, '..', 'storage' ];
    const dirs = [
        path.join(__dirname, '..', 'keys'),
        path.join(...storageBase, 't'),
        path.join(...storageBase, 'v'),
        path.join(...storageBase, 'u', 'a', 'o'),
        path.join(...storageBase, 'u', 'a', 's'),
    ];

    dirs.forEach(dir => {
        exists(dir, (exist) =>{
            if (!exist) mkdir(dir, { recursive: true }, (err) => { });
        });
    });
}
