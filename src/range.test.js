const {
    calculateEndTime,
    findFirstSpot,
    hasSpaceForTask,
    isAfterWorkStart,
    isBeforeWorkEnd
} = require("./range");

describe("check for date ranges", () => {
    const range = (start, end) => ({ start, end });

    describe("hasSpaceForTask", () => {
        test("has space for task", () => {
            expect(hasSpaceForTask(range(3, 9), range(4, 8))).toBe(true);
        });

        test("has no space for task", () => {
            expect(hasSpaceForTask(range(3, 6), range(3, 6))).toBe(false);
            expect(hasSpaceForTask(range(3, 7), range(4, 7))).toBe(false);
            expect(hasSpaceForTask(range(3, 7), range(3, 6))).toBe(false);
        });
    });

    describe("calculateEndTime", () => {
        test("calculate end time from start time and duration", () => {
            const date = new Date(2019, 9, 31, 23, 30);
            const expectedDate = new Date(2019, 10, 1, 0, 3);

            expect(calculateEndTime(date, 33).toString()).toBe(expectedDate.toString());
        });
    });

    describe("isBeforeWorkEnd", () => {
        test("task ends before work day", () => {
            const taskEnd = new Date(2019, 10, 1, 17, 30);
            const workDayEnd = new Date(2019, 10, 1, 17, 30);
            const workDayEnd2 = new Date(2019, 10, 1, 17, 34);

            expect(isBeforeWorkEnd(taskEnd, workDayEnd)).toBe(true);
            expect(isBeforeWorkEnd(taskEnd, workDayEnd2)).toBe(true);
        });

        test("task ends after work day", () => {
            const taskEnd = new Date(2019, 10, 1, 17, 30);
            const workDayEnd = new Date(2019, 10, 1, 17, 29);

            expect(isBeforeWorkEnd(taskEnd, workDayEnd)).toBe(false);
        });
    });

    describe("isAfterWorkStart", () => {
        test("task will start after work day start", () => {
            const taskStart    = new Date(2019, 10, 1, 8, 31);
            const taskStart2   = new Date(2019, 10, 1, 8, 30);
            const workDayStart = new Date(2019, 10, 1, 8, 30);

            expect(isAfterWorkStart(taskStart, workDayStart)).toBe(true);
            expect(isAfterWorkStart(taskStart2, workDayStart)).toBe(true);
        });

        test("false if task will start before work day start", () => {
            const taskStart    = new Date(2019, 10, 1, 8, 29);
            const workDayStart = new Date(2019, 10, 1, 8, 30);

            expect(isAfterWorkStart(taskStart, workDayStart)).toBe(false);
        });
    });

    describe("findFirstSpot", () => {
        test("select first spot for required time", () => {
            const spots = [
                {
                    end:   new Date(2019, 10, 1, 8, 39),
                    start: new Date(2019, 10, 1, 8, 29)
                },
                {
                    end:   new Date(2019, 10, 1, 8, 45),
                    start: new Date(2019, 10, 1, 8, 40)
                },
                {
                    end:   new Date(2019, 10, 1, 9, 10),
                    start: new Date(2019, 10, 1, 8, 59)
                },
            ];
            const expected = {
                end:   new Date(2019, 10, 1, 8, 56),
                start: new Date(2019, 10, 1, 8, 46)
            };
            const expectedEnd = {
                end:   new Date(2019, 10, 1, 9, 41),
                start: new Date(2019, 10, 1, 9, 11)
            };

            expect(findFirstSpot(spots, 10)).toStrictEqual(expected);
            expect(findFirstSpot(spots, 30)).toStrictEqual(expectedEnd);
        });
    });
});
