const { v4: uuidv4 } = require('uuid');

const projectFileRecord = (projectId, filename, originalname, additional, addedBy) => {
    return {
        uuid: uuidv4(),
        project_id: projectId,
        file_name: filename,
        original_file_name: originalname,
        additional,
        added_by: addedBy,
    };
};

const getCommentRecord = (item, users) => {
    const additional = JSON.parse(item.additional);

    return {
        uuid: item.uuid,
        project_file_id: item.project_file_id,
        comment: item.comment,
        start: additional.start,
        end: additional.end,
        added_by: users[item.added_by],
        created_by: item.added_by,
        parent_id: item.parent_id,
        created_at: (new Date(item.created_at).getTime()),
    };
};

const getThumbId = (thumb) => {
    return (thumb !== undefined) ? thumb.uuid : undefined;
};

const getGroupIds = (groupIds) => {
    return (groupIds !== undefined) ? groupIds.map(grp => grp.group_id) : [];
};

const cutDescription = (description) => {
    if (description && description.length > 147) {
        const desc = description.substring(0, 147) ;

        description = `${desc}...`;
    }

    return description;
};

const getProjectIds = (projectGroups) => {
    const projectIds = projectGroups.map(proj => proj.project_id);

    const uniqueIds = new Set([ ...projectIds ]);

    return [ ...uniqueIds ];
};

module.exports = {
    projectFileRecord,
    getCommentRecord,
    getThumbId,
    getGroupIds,
    cutDescription,
    getProjectIds,
};
