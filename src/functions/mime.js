const { extname } = require('path');

const getMimeType = (fileName) => {
    let type = 'svg+xml';
    const ext = extname(fileName);

    if (ext === '.png') {
        type = 'png';
    } else if (ext === '.jpg' || ext === '.jpeg') {
        type = 'jpeg';
    }

    return `image/${type}`;
};

module.exports = {
    getMimeType
};
