
const timeThings = () => {
    const timestamp = parseInt(Date.now());

    return {
        exp: timestamp + (24 * 60 * 60 * 1000),
        iat: timestamp,
    };
};

module.exports = {
    timeThings,
};
