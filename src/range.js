const {
    addMinutes, isAfter, isBefore, isEqual
} = require('date-fns');

const hasSpaceForTask = (timeBetween, neededSpot) => {
    return timeBetween.start < neededSpot.start && neededSpot.end < timeBetween.end;
};

const calculateEndTime = (date, minutes) => {
    return addMinutes(date, minutes);
};

const isBeforeWorkEnd = (taskEnd, workDayEnd) => {
    return isBefore(taskEnd, workDayEnd) || isEqual(taskEnd, workDayEnd);
};

const isAfterWorkStart = (taskStart, workDayStart) => {
    return isAfter(taskStart, workDayStart) || isEqual(taskStart, workDayStart);
};

const findFirstSpot = (spots, durationForNewSpot) => {
    const spotCnt = spots.length;
    let spotTime = { };

    const spotData = (end, start) => ({ end, start });

    for (let idx = 1; idx <= spotCnt; idx++) {
        const spot = spots[ idx - 1 ];
        const spotStart = calculateEndTime(spot.end, 1);
        const nextIdx   = idx;

        const end = calculateEndTime(spotStart, durationForNewSpot);

        if (nextIdx < spotCnt) {
            const nextSpot = spots[ nextIdx ];

            const neededSpot  = spotData(end, spotStart);
            const timeBetween = spotData(nextSpot.start, spot.end);

            if (hasSpaceForTask(timeBetween, neededSpot)) {
                spotTime = spotData(end, spotStart);
                break;
            }
        }

        if (nextIdx === spotCnt) {
            spotTime = spotData(end, spotStart);
            break;
        }
    }

    return spotTime;
};

module.exports = {
    calculateEndTime,
    findFirstSpot,
    hasSpaceForTask,
    isAfterWorkStart,
    isBeforeWorkEnd
};
