
const stringLength = val => val && val.length <= 255;

module.exports = {
    stringLength
}
