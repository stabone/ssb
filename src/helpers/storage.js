const path = require('path');

const avatarParts = [ 'storage', 'u', 'a' ];

const videoPathDir = path.join('storage', 'v');
const thumbPathDir = path.join('storage', 't');
const avatarPathDir = path.join(...avatarParts, 'o');
const avatarSamllPathDir = path.join(...avatarParts, 's');

const privateKeyPath = path.join('keys', 'private.key');
const publicKeyPath = path.join('keys', 'public.key');

const videoPath = (video) => path.join(videoPathDir, video);
const thumbPath = (thumb) => path.join(thumbPathDir, thumb);
const avatarPath = (file) => path.join(avatarPathDir, file);
const avatarSmallPath = (file) => path.join(avatarSamllPathDir, file);

module.exports = {
    videoPathDir,
    thumbPathDir,

    avatarPathDir,
    avatarPath,

    avatarSamllPathDir,
    avatarSmallPath,

    videoPath,
    thumbPath,

    privateKeyPath,
    publicKeyPath,
};
