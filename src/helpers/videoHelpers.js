const fs   = require('fs');
const path = require('path');

const ffmpeg  = require('fluent-ffmpeg');

const  deleteIfExists = (file) => {
    if (fs.existsSync(file)) {
        fs.unlinkSync(file);
    }
};

module.exports = {
    videoUploadDir: 'data/v/',
    thumbUploadDir: 'data/t/',

    videoUrl: '/lib/v/',
    thumbUrl: '/lib/t/',

    getVideoUrl(recordId) {
        return `${this.videoUrl}${recordId}`;
    },

    getThumbUrl(recordId) {
        return `${this.thumbUrl}${recordId}`;
    },

    getFullVideoPath(filename) {
        return `${this.videoUploadDir}${filename}`;
    },

    getFullThumbPath(filename) {
        return `${this.thumbUploadDir}${filename}`;
    },

    getFallbackThumb() {
        return this.getFullThumbPath('no-image.png');
    },

    isValidMimeType(type) {
        const fileTypes = [
            'video/mp4',  // .mp4 or .m4v
            'video/ogg',  //.ogv
            'video/webm', // .webm
        ];

        return fileTypes.includes(type);
    },

    getFileReadingInfo(headers, fileStats, filename) {
        const { range } = headers;
        const { size }  = fileStats;

        const start = Number((range || '').replace(/bytes=/, '').split('-')[0]);
        const end   = size - 1;
        const chunkSize = (end - start) + 1;

        const contentType = this.getContentTypeByFileName(filename);

        return { start, end, size, chunkSize, contentType };
    },

    getContentTypeByFileName(filename) {
        const ext = path.extname(filename);

        if (ext == '.mp4' || ext == '.m4v') {
            return 'video/mp4';
        }

        if (ext == '.ogv') {
            return 'video/ogg';
        }

        if (ext == '.webm') {
            return 'video/webm';
        }

        return 'video/webm';
    },

    getChunkHeader({ start, end, size, chunkSize, contentType }) {
        return {
            'Content-Range': `bytes ${start}-${end}/${size}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunkSize,
            'Content-Type': contentType
        };
    },

    getVideoDuration(fileInfo) {
        return new Promise((resolve, reject) => {
            const filename = this.getFullVideoPath(fileInfo.filename);

            ffmpeg.ffprobe(filename, (err, metadata) => {
                if (err) {
                    return reject(err);
                }

                resolve(metadata.format.duration);
            });
        });
    },

    createFileName(file) {
        const extension = path.extname(file.originalname);

        let originalFileName = file.originalname;

        let cutLength = (originalFileName.length > 10 ? 10 : originalFileName.length),
        tmpFileName = originalFileName.substr(0, cutLength) + '-' + Date.now();

        return Buffer.from(tmpFileName).toString('hex') + extension;
    },

    deleteImageIfExists(file) {
        const fullPath = this.getFullThumbPath(file);

        deleteIfExists(fullPath);
    },

    deleteVideoIfExists(file) {
        const fullPath = this.getFullVideoPath(file);

        deleteIfExists(fullPath);
    },

    durationToHuman(durationInSeconds) {
        const minutes = parseInt(durationInSeconds / 60);
        const seconds = durationInSeconds % 60;

        const minPrefix =  minutes < 10 ? '0' : '';
        const secPrefix =  seconds < 10 ? '0' : '';

        return `${minPrefix}${minutes}:${secPrefix}${seconds}`;
    },

    getOffset(page, limit) {
        let offset = 0;

        if (page > 1) {
            offset = limit * (page - 1);
        } else {
            offset = 0;
        }

        return offset;
    },

    parseVideos(videos) {
        return videos.map(video => {
            let videoData = video.dataValues;

            const { duration } = videoData;

            videoData.image = this.getThumbUrl(videoData.id);
            videoData.video = this.getVideoUrl(videoData.id);

            videoData.duration_human = this.durationToHuman(duration);

            return videoData;
        });
    }
};