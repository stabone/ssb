

const signOptions = (subject) => ({
    issuer: "My corp",
    subject,
    audience: "www.cookie.com",
    expiresIn:  "12h",
    algorithm:  "RS256"
});

module.exports = {
    signOptions
};