
const getDataFromRequest = (body, fields) => {
    let data = { };

    fields.forEach(field => {
        if (body[field]) data[field] = body[field];
    });

    return data;
};

module.exports = {
    getDataFromRequest
};
