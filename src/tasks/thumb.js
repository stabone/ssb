const ffmpeg  = require('fluent-ffmpeg');

const { videoPath, thumbPath } = require('../helpers/storage');


const getVideoDuration = (filename) => {
    return new Promise((resolve, reject) => {
        ffmpeg.ffprobe(filename, (err, metadata) => {
            if (err) return reject(err);

            resolve(metadata.format.duration);
        });
    });
};


const createThumbnail = async (videoFile, thumbUploadDir) => {
    let duration = await getVideoDuration(videoFile);
    const filename = '%b-thumb-%00i.jpg';

    duration = parseInt(duration);

    return new Promise((resolve, reject) => {
        ffmpeg({ source: videoFile })
            .on('filenames', function (filenames) {
                resolve(filenames);
            })
            .on('end', async (par, ams) => {
                // console.log('END', par, ams)
            })
            .on('error', (err) => {
                errors.log(err);
                reject(err)
            })
            .takeScreenshots({
                    count: 1,
                    filename,
                    timemarks: [ duration / 4, duration / 2  ],
                    size: '?x320'
                }, thumbUploadDir);
    });
};

module.exports = {
    createThumbnail
};
