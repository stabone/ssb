require('dotenv').config();

const path = require('path');

const {
    DB_HOST, DB_NAME, DB_USER, DB_PASS, ENVIRONMENT
} = process.env;
const environment = ENVIRONMENT || 'development';



module.exports = {
    development: {
        client: 'mysql',
        connection: {
            host: DB_HOST,
            user: DB_USER,
            // username: 'root',
            password: DB_PASS,
            database: DB_NAME
        },
        pool: {
            min: 2,
            max: 10
        },
        migrations: {
            tableName: 'knex_migrations',
            directory: path.join(__dirname, 'migrations')
        },
    },
    production: {
        client: 'mysql',
        connection: {
            host: DB_HOST,
            database: DB_NAME,
            username: DB_USER,
            password: DB_PASS,
        },
        migrations: {
            tableName: 'knex_migrations',
            directory: path.join(__dirname, 'migrations')
        },
    }
};
